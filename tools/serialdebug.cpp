#include "serialdebug.h"
#include "ui_serialdebug.h"
//https://www.cnblogs.com/feiyangqingyun/p/3483764.html
//https://www.cnblogs.com/hanford/p/6048325.html

SerialDebug::SerialDebug(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SerialDebug)
{
    ui->setupUi(this);

    //初始化串口设备
    serial = new QSerialPort(this);
    connect(serial, SIGNAL(readyRead()), this, SLOT(recvSlot()));

    //连接错误处理
    connect(serial,SIGNAL(error(QSerialPort::SerialPortError)),
            this,SLOT(errHandler(QSerialPort::SerialPortError)));

    //初始化界面
    initGui();
}

SerialDebug::~SerialDebug()
{
    serial->clear();
    serial->close();
    delete ui;
}


void SerialDebug::errHandler(QSerialPort::SerialPortError errNum)
{
    switch (errNum) {
    case QSerialPort::OpenError:
        ui->errLab->setText("An error occurred while attempting to open an already opened device in this object");
        break;
    default:
        break;
    }
}

void SerialDebug::initGui()
{
    QStringList baudList;//波特率
    QStringList parityList;//校验位
    QStringList dataBitsList;//数据位
    QStringList stopBitsList;//停止位

    //根据连接串口显示可用串口号
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        ui->portBox->addItem(info.portName());
    }

    //波特率
    baudList<<"1200"<<"2400"<<"4800"<<"9600"
          <<"19200"<<"38400"<<"57600"<<"115200";
    ui->baudBox->addItems(baudList);
    ui->baudBox->setCurrentIndex(7);  //QSerialPort::Baud1200 = 1200

    //奇偶校验
    parityList<<"None"<<"Even"<<"Odd"<<"Space"<<"Mark";
    ui->parityBox->addItems(parityList);
    ui->parityBox->setCurrentIndex(QSerialPort::NoParity);

    //数据位
    dataBitsList<<"5"<<"6"<<"7"<<"8";
    ui->dataBox->addItems(dataBitsList);
    ui->dataBox->setCurrentIndex(3);  //QSerialPort::Data5 = 5

    //停止位
    stopBitsList<<"1"<<"1.5"<<"2";
    ui->stopBox->addItems(stopBitsList);
    ui->stopBox->setCurrentIndex(0);  //QSerialPort::OneAndHalfStop =  3
}

void SerialDebug::on_openBtn_clicked()
{
    if(!serial->isOpen()){
        serial->setPortName(ui->portBox->currentText());

        if(!serial->open(QIODevice::ReadWrite)){
            ui->errLab->setText("打开串口失败");
            qDebug()<<serial->errorString();
            return;
        }

        setSerialAttr();
        ui->openBtn->setText("关闭串口");
    }else{
        serial->clear();
        serial->close();
        ui->openBtn->setText("打开串口");
    }
}

void SerialDebug::setSerialAttr()
{
    serial->setBaudRate(ui->baudBox->currentText().toInt());
    serial->setParity(QSerialPort::Parity(ui->parityBox->currentIndex()));
    serial->setDataBits(QSerialPort::DataBits(ui->dataBox->currentText().toInt()));

    QString stopStr = ui->stopBox->currentText();
    if(stopStr == "1"){
        serial->setStopBits(QSerialPort::OneStop);
    }else if (stopStr == "1.5") {
        serial->setStopBits(QSerialPort::OneAndHalfStop);
    }else{
        serial->setStopBits(QSerialPort::TwoStop);
    }

    serial->setRequestToSend(true);             //设置 RTS 为高电平
    serial->setDataTerminalReady(true);         //设置 DTR 为高电平
    serial->setFlowControl(QSerialPort::NoFlowControl);

    qDebug()<<"setPortName="<<serial->portName();
    qDebug()<<"setBaudRate="<<serial->baudRate();
    qDebug()<<"setDataBits="<<serial->dataBits();
    qDebug()<<"setParity="<<serial->parity();
    qDebug()<<"setStopBits="<<serial->stopBits();
}

void SerialDebug::recvSlot()
{
    //https://blog.csdn.net/dengdew/article/details/79065608
    QByteArray buf;
    buf = serial->readAll();
    ui->recvEdit->appendPlainText(QString(buf.toHex()));
}
