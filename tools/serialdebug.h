#ifndef SERIALDEBUG_H
#define SERIALDEBUG_H

#include <QtWidgets>
#include <QSerialPort>
#include <QSerialPortInfo>

namespace Ui {
class SerialDebug;
}

class SerialDebug : public QWidget
{
    Q_OBJECT

public:
    explicit SerialDebug(QWidget *parent = 0);
    ~SerialDebug();

private:
    Ui::SerialDebug *ui;
    QSerialPort *serial;  //串口设备

    void initGui();
    void setSerialAttr();

private slots:
    void errHandler(QSerialPort::SerialPortError);
    void on_openBtn_clicked();
    void recvSlot();
};

#endif // SERIALDEBUG_H
